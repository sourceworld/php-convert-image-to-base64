# PHP Convert Image To Base64

```php

<?php

//Image URL or PATH
	// http://example.com/image.jpg
	// ../images/image.jpg

$image_url = "http://example.com/image.jpg";

//Accepted image types
$image_types = array('png','jpg','jpeg','gif','tif','bmp');

//Get image extension
$image_extension = pathinfo($image_url,PATHINFO_EXTENSION);

//Check if is accepted image type
if(in_array($image_extension,$image_types)){
	
	$image_base64 = "data:image/".$image_extension.";base64,".base64_encode(file_get_contents($image_url));
	
	//Your base64 image
	echo $image_base64;
	
}

?>

```